mmcore.IntegrationFactory.register('CoreMetrics', {
    defaults: {
        tags: {
            element: 'cmCreateElementTag',
            pageview: 'cmCreatePageviewTag',
            productview: 'cmCreateProductviewTag'
        }
    },

    validate: function (data) {
        if(!data.campaign)
            return 'No campaign.';

        if (!window.sessionStorage)
            return 'No sessionStorage available.';

        if(!data.tags)
            data.tags = ['element'];

        data.tags = data.tags.push ? data.tags : [data.tags];

        for (var i = 0; i < data.tags.length; i ++) {
            if (!this.defaults.tags[data.tags[i]]) {
                return 'Invalid tag type: ' + data.tags[i] + '. Must be: element, pageview, or productview.';
            }
        }

        return true;
    },

    check: function (data) {
        return window.cmCreateElementTag;
    },

    exec: function (data) {
        var item, storage, session, now, experience, tags;

        storage = window.sessionStorage || {
            cookie: 'mm_sessionStorage',

            getItem: function (name) {
                var storedItems = JSON.parse(mmcore.GetCookie(this.cookie, 1) || '{}');
                return storedItems[name];
            },

            setItem: function (name, value) {
                var storedItems = JSON.parse(mmcore.GetCookie(this.cookie, 1) || '{}');
                storedItems[name] = value;
                mmcore.SetCookie(cookie, JSON.stringify(storedItems), 0, 1);
            }
        };

        item = 'mm_CM_' + data.campaign.replace(/\W/g,''),
        session = storage.getItem(item),
        now = new Date().getTime(),
        experience = data.campaignInfo.replace(/^.+?=/, ''),
        tags = data.tags.push ? data.tags : [data.tags];

        storage.setItem(item, now);

        if (session && (now - session < 18E5)) {
            mmcore.EH({message:'[' + data.campaign + '] Not sending again in same session.'});
            return true;
        }

        for (var i = 0; i < tags.length; i ++) {
            var type = tags[i],
                args = [experience],
                mode = data.isProduction ? 'MM_Prod_' : 'MM_Sand_';

            if (type === 'productview') {
                args.push(experience);
            }

            args.push(mode + data.campaign);
            window[this.defaults.tags[type]].apply(window, args);
        }

        if (typeof data.callback === 'function') data.callback.call(null, data.campaignInfo);
        return true;
    }
});